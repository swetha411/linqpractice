﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;

namespace MoreLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>
			{
				new Product { Name = "Clock", Cost = 20.99, Category = "Decor" },
				new Product { Name = "Desk", Cost = 150.00, Category = "Furniture" },
				new Product { Name = "Mirror", Cost = 49.99, Category = "Decor" },
				new Product { Name = "Chair", Cost = 120.99, Category = "Furniture" },
				new Product { Name = "Lamp", Cost = 150.00, Category = "Decor" }
			};
            //Example 1: Filtering a list of products
            

            //Example 2 - Using Anonymous types
            
            //Example 3 - First, Single and Take(n)
            int[] x = {3,4,5,10,15,16,20};

            //Example 4 - Joining two collections
            List<Section> sections = new List<Section> {
                new Section { Teacher = "Swetha", SectionNumber = 1 },
                new Section { Teacher = "Dirk", SectionNumber = 2 }
                };
            List<Student> students = new List<Student> {
                new Student { Name = "Carl", Grade = 45, ID = 1, SectionNumber = 1 },
                new Student { Name = "Ada", Grade = 72, ID = 2, SectionNumber = 2 },
                new Student { Name = "Grace", Grade = 80, ID = 3, SectionNumber = 1 },
                new Student { Name = "Tim", Grade = 74, ID = 4, SectionNumber = 2 }
                };
            
            //Example 5 - Grouping
            

        }
    }
     

    class Product {
        public string Name { get; set; }
        public double Cost { get; set; }
        public string Category {get; set;}
    }

    class Student
    {
        public string Name { get; set;}
        public int ID {get; set;}
        public int Grade { get; set;}
        public int SectionNumber {get; set;}
    }
    class Section
    {
        public string Teacher {get; set;}
        public int SectionNumber{get; set;}
    }

}