﻿using System;
using System.Collections.Generic;

namespace LinqExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Example 1 - Counting even numbers in a list
            var numbers = new List<int>{1,2,3,4,5,6,7};

            //Example 2 - Counting unvaccinated pets in an array
            Pet[] pets = { new Pet { Name="Barley", Vaccinated=true, Age = 1 },
            new Pet { Name="Boots", Vaccinated=false, Age = 5  },
            new Pet { Name="Whiskers", Vaccinated=false, Age = 3  } };

            //Example 3: Linq expression to filter and sort a list of strings
            List<string> words = new List<string>{"shelf", "book", "clock", "desk", "candle", "pen"};
            
            //Example 4: Linq expression to filter an array of city name
            string[] cities = {"London", "Vienna", "Paris", "Linz", "Brussels"};
            
            //Exercise: Linq expression to filter a list of Students
            List<Student> students = new List<Student>
			{
				new Student { Name = "Ada", ID = 123456, Grade = 95 },
				new Student { Name = "Carl", ID = 456789, Grade = 42 },
				new Student { Name = "Grace", ID = 234789, Grade = 80 }
			};
            //Exercise: Linq expression to filter the list of Pets
            



        }
    }
    class Pet {
        public string Name { get; set; }
        public bool Vaccinated { get; set; }
        public int Age {get; set;}
}

    class Student
    {
        public string Name { get; set;}
        public int ID {get; set;}
        public int Grade { get; set;}
    }
}
